from scipy.optimize import minimize
from multilateration import *
from geography import *


def f_baseline(d):
    return d / 100.


def get_all_bestlines(distances, rtts):
    size = np.size(distances[0])
    bestlines = np.zeros(shape=(size, 2))

    for anchor_index in range(size):
        # rtts_i = np.minimum(rtts[anchor_index, :], rtts[:, anchor_index])  # smallest rtt regardless of ping direction
        rtts_i = rtts[anchor_index, :]
        bestlines[anchor_index, :] = get_bestline(distances[anchor_index, :], rtts_i)
    return bestlines


def get_all_bestlines_without_index(distances, rtts, without):
    rtts = np.copy(rtts)
    rtts[without, :] = -1
    rtts[:, without] = -1
    return get_all_bestlines(distances, rtts)


def get_bestline(distances, rtts):
    valid_indices = np.where(rtts != -1)  # dont count anchor in question
    distances = distances[valid_indices]
    rtts = rtts[valid_indices]

    if len(distances) == 0 or len(rtts) == 0:
        return -1, -1

    bestline = fit_below(distances, rtts, (0, np.inf))
    return bestline


# We find the bestline by fitting below all points within bounds
# This is adapted from : https://stackoverflow.com/questions/14490400/line-fitting-below-points
def fit_below(x, y, b_bounds):
    non_zeros = np.where(x > 0)  # remove zeros to avoid divide by 0 (won't affect result)
    x = x[non_zeros]
    y = y[non_zeros]

    def error_function_2(b, x, y):
        m = np.min((y - b) / x)
        return np.sum((y - m * x - b)**2)

    b = minimize(error_function_2, np.array([0, ]), bounds=(b_bounds,), args=(x, y)).x[0]
    m = np.min((y - b) / x)
    return np.array([m, b])


def bestline_error(m_b, distance_points, rtt_points):
    m = m_b[0]
    b = m_b[1]

    rtt_bestline = (m * distance_points) + b
    return np.sum((rtt_points - rtt_bestline)**2)


def get_rtt_from_list(rtt_list):
    return -1 if np.size(rtt_list) == 0 else np.min(rtt_list)


def get_estimated_radius(rtts, bestlines, test_index, other_index):
    possible_rtts = filter(lambda t: t > 0, [rtts[test_index][other_index], rtts[other_index][test_index]])
    if len(possible_rtts) == 0:
        raise Exception("no rtt information from this host")

    rtt = min(possible_rtts)
    m_other = bestlines[other_index][0]
    b_other = bestlines[other_index][1]

    estimated_radius = (rtt - b_other)/m_other
    return estimated_radius


def do_cbg_on_anchor(anchors, distances, rtts, anchor_index):

    def valid_other_index(i):
        return i != anchor_index and \
               max(rtts[anchor_index][i], rtts[i][anchor_index]) > 0

    bestlines = get_all_bestlines_without_index(distances, rtts, anchor_index)
    other_indices = np.array(filter(valid_other_index, range(len(anchors))))
    other_longitudes = np.array([anchors[i]["geometry"]["coordinates"][0] for i in other_indices])
    other_latitudes = np.array([anchors[i]["geometry"]["coordinates"][1] for i in other_indices])
    other_radii = np.array([get_estimated_radius(rtts, bestlines, anchor_index, i) for i in other_indices])

    try:
        multilateration = Multilateration(other_latitudes, other_longitudes, other_radii,
                                          anchors[anchor_index]["country"])
    except MultilaterationException:
        return -1, -1, -1

    actual_coordinates = anchors[anchor_index]["geometry"]["coordinates"]
    estimated_coordinates = [multilateration.getCentroid().long, multilateration.getCentroid().lat]
    error_distance = geo_distance(*(actual_coordinates + estimated_coordinates))
    area = multilateration.getArea()

    return error_distance, area, multilateration



