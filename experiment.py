import cbg
import data_collection as collect
import datetime
import numpy as np
import matplotlib.pyplot as plt

"""
User Settings
"""


CONTINENT = "EU"
ANCHOR_SAMPLE_SIZE = 55
START_TIME = datetime.datetime(year=2018, month=12, day=14, hour=0, minute=0, second=0)
END_TIME = datetime.datetime(year=2018, month=12, day=14, hour=0, minute=4, second=0)

DO_GET_ANCHORS = False  # gets all anchors in continent and corresponding measurement urls
DO_SAMPLE = False  # take random sample of anchors with valid measurements
DO_PINGS = False  # get pings within time span from anchors


"""
Plotting
"""


def show_plots():
    plt.show()


def plot_rtt_vs_distance(distances, rtts, figure=None, fmt="k.", **kwargs):
    if figure is None:
        figure = plt.figure()

    valids = np.where(rtts != -1)
    plt.plot(distances[valids], rtts[valids], fmt, **kwargs)
    return figure


def plot_bestline(distances, bestline, figure=None, fmt="b-", **kwargs):
    if figure is None:
        figure = plt.figure()

    bestline_d = np.array([0, distances.max() * 1.2])
    bestline_rtt = (bestline[0] * bestline_d) + bestline[1]

    plt.plot(bestline_d, bestline_rtt, fmt, **kwargs)
    return figure


def plot_baseline(distances, figure=None, fmt="k-", **kwargs):
    if figure is None:
        figure = plt.figure()

    baseline = np.array([[0, distances.max() * 1.2],
                         [0, cbg.f_baseline(distances.max() * 1.2)]])
    plt.plot(baseline[0], baseline[1], fmt, **kwargs)
    return figure


def plot_sample_anchor_scatter(distances, rtts, bestline, figure=None, title=None, scattersize=10):
    if figure is None:
        figure = plt.figure()

    plot_rtt_vs_distance(distances, rtts, figure=figure, fmt="k.", markersize=scattersize)
    plot_bestline(distances, bestline, figure=figure, fmt="b-", markersize=10)
    plot_baseline(distances, figure=figure, fmt="k-", markersize=10)
    plt.xlabel("Distance (km)")
    plt.ylabel("RTT (ms)")

    if title is not None:
        plt.title(title)

    return figure


def plot_cumulative_probability_distribution(x, figure=None, bins=10):
    if figure is None:
        figure = plt.figure()

    hist, buckets = np.histogram(x, range=(0., x.max()), bins=bins)
    hist_normed = hist.astype("float")/np.sum(hist)
    cum_prob = np.cumsum(hist_normed)
    x_upper_bucket = buckets[1:]

    x_upper_bucket = np.append(0, x_upper_bucket)  # add (0, 0) to plot
    cum_prob = np.append(0, cum_prob)

    plt.plot(x_upper_bucket, cum_prob, 'k-')
    plt.xlim((0, x.max()))
    plt.ylim(0, 1)
    plt.yticks(np.arange(0, 1, 0.1))
    plt.ylabel("Cumulative Probability")
    return figure


"""
Experiments
"""


def multilateration_test(anchor_index):
    anchors, measurements, distances, rtts = collect.collect_data(continent=CONTINENT,
                                                                  anchor_sample_size=ANCHOR_SAMPLE_SIZE,
                                                                  start_time=START_TIME,
                                                                  end_time=END_TIME,
                                                                  do_get_anchors=DO_GET_ANCHORS,
                                                                  do_sample=DO_SAMPLE,
                                                                  do_pings=DO_PINGS)

    for i in range(len(anchors)):
        error, area, multilateration = cbg.do_cbg_on_anchor(anchors, distances, rtts, i)
        if error != -1:
            multilateration.plot()
            break
        print "Error: {:.2f}\tArea: {:.2f}\t\tAnchor ({} of {})".format(error, area, i + 1, len(anchors))


def cumulative_distribution_test(bins=10):
    anchors, measurements, distances, rtts = collect.collect_data(continent=CONTINENT,
                                                                  anchor_sample_size=ANCHOR_SAMPLE_SIZE,
                                                                  start_time=START_TIME,
                                                                  end_time=END_TIME,
                                                                  do_get_anchors=DO_GET_ANCHORS,
                                                                  do_sample=DO_SAMPLE,
                                                                  do_pings=DO_PINGS)

    errors = []
    areas = []
    for i in range(len(anchors)):
        error, area, _ = cbg.do_cbg_on_anchor(anchors, distances, rtts, i)
        errors.append(error)
        areas.append(area)
        print "Error: {:.2f}\tArea: {:.2f}\t\tAnchor ({} of {})".format(error, area, i + 1, len(anchors))

    errors = np.array(errors)
    areas = np.array(areas)
    valids = np.where(errors != -1)
    print "Invalids: {}".format(len(errors) - len(valids[0]))

    errors = errors[valids]
    areas = areas[valids]

    # Plot Error Distance
    error_fig = plot_cumulative_probability_distribution(errors, bins=bins)
    plt.figure(error_fig.number)
    plt.xlabel("Error Distance (km)")

    # Area
    area_fig = plot_cumulative_probability_distribution(np.log10(areas), bins=bins)
    plt.figure(area_fig.number)
    plt.xlabel(r"Area $\log_{10}$ (km$^2$)")


def sample_anchor_scatter_test(anchor_index):
    anchors, measurements, distances, rtts = collect.collect_data(continent=CONTINENT,
                                                                  anchor_sample_size=ANCHOR_SAMPLE_SIZE,
                                                                  start_time=START_TIME,
                                                                  end_time=END_TIME,
                                                                  do_get_anchors=DO_GET_ANCHORS,
                                                                  do_sample=DO_SAMPLE,
                                                                  do_pings=DO_PINGS)

    bestlines = cbg.get_all_bestlines(distances, rtts)
    i = anchor_index
    title = "Scatter Plot for a Sample Anchor ({}, {})".format(anchors[i]["city"], anchors[i]["country"])
    plot_sample_anchor_scatter(distances[i], rtts[i], bestlines[i], title=title, scattersize=4)


def all_anchors_scatter_test():
    anchors, measurements, distances, rtts = collect.collect_data(continent=CONTINENT,
                                                                  anchor_sample_size=ANCHOR_SAMPLE_SIZE,
                                                                  start_time=START_TIME,
                                                                  end_time=END_TIME,
                                                                  do_get_anchors=DO_GET_ANCHORS,
                                                                  do_sample=DO_SAMPLE,
                                                                  do_pings=DO_PINGS)
    distances = distances.flatten()
    rtts = rtts.flatten()
    bestline = cbg.get_bestline(distances, rtts)
    title = "Scatter Plot for {} Sample Anchors".format(len(anchors))
    plot_sample_anchor_scatter(distances, rtts, bestline, title=title, scattersize=1)


if __name__ == "__main__":
    # sample_anchor_scatter_test(5)
    # all_anchors_scatter_test()
    cumulative_distribution_test(bins=25)
    # multilateration_test(0)
    plt.show()






