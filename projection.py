"""

Projection Engine for Project 2
Constraint-Based Geolocation of Internet Hosts

Created by Justin Wang (jtw57) and Dhruv K Patel (dkp13)

Description: maps internet hosts and ranges onto a Mercator projection of Earth

"""

### Import Statements ###
# MatPlotLib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

# Cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature

# Shapely
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import shapely.affinity

import pyproj
from functools import partial
from shapely.ops import transform

# Numpy
import numpy as np

class Projection:

    def __init__( self, country_code ):

        self.dim = [1500, 1000]
        self.dpi = 128
        self.resolution = '50m'
        self.glob = False

        if country_code.lower() == 'us':
            self.bounds = [-130.0, -60.0, 20.0, 50.0]
        elif country_code.lower() == 'eu':
            self.bounds = [-20.0, 50.0, 30.0, 65.0]
        else:
            self.glob = True

        self.proj_wgs84 = pyproj.Proj(init='epsg:4326')

        return

    def geodesic_point_buffer( self, lat, lon, km ):
        # Azimuthal equidistant projection
        aeqd_proj = '+proj=aeqd +lat_0={lat} +lon_0={lon} +x_0=0 +y_0=0'
        project = partial(
            pyproj.transform,
            pyproj.Proj(aeqd_proj.format(lat=lat, lon=lon)),
            self.proj_wgs84)
        buf = Point(0, 0).buffer(km * 1000)  # distance in metres
        return transform(project, buf).exterior.coords[:]

    def genMercator( self, hosts, poly_vertices, centroid ):

        proj = ccrs.Mercator.GOOGLE

        fig = plt.figure(figsize=(self.dim[0]/self.dpi, self.dim[1]/self.dpi), dpi=self.dpi)
        ax = fig.add_subplot(1, 1, 1, projection=proj)

        if self.glob:
            ax.set_global()
        else:
            ax.set_extent(self.bounds)

        # ax.add_feature(cfeature.LAND)
        # ax.add_feature(cfeature.OCEAN)
        # ax.add_feature(cfeature.COASTLINE)
        # ax.add_feature(cfeature.BORDERS, linestyle=':')
        # ax.add_feature(cfeature.LAKES, alpha=0.5)
        # # ax.add_feature(cfeature.RIVERS)
        # ax.add_feature(cfeature.STATES)

        ax.add_feature(cfeature.NaturalEarthFeature('physical', 'land', self.resolution, edgecolor='black', facecolor=cfeature.COLORS['land']))
        ax.add_feature(cfeature.NaturalEarthFeature('cultural', 'admin_0_countries', self.resolution, edgecolor='black', facecolor='none'))
        ax.add_feature(cfeature.NaturalEarthFeature('cultural', 'admin_1_states_provinces_lines', self.resolution, edgecolor='gray', facecolor='none'))


        # Center Lat/Long of United States: 39.833333, -98.583333

        # Plotting All Circles

        for host in hosts:
            # circle = Point(host.long, host.lat).buffer(host.r_degrees)

            b = self.geodesic_point_buffer(host.lat, host.long, host.radius)
            poly = Polygon(b)
            ax.add_geometries([poly], ccrs.PlateCarree(), facecolor='#C8A2C8', alpha=0.25)
            # break;
            # ax.add_patch(mpatches.Circle(xy=[host.long, host.lat], radius=host.r_degrees, color='blue', alpha=0.3, zorder=30, transform=ccrs.PlateCarree()))




        # Generating Polygon

        poly_vert_flat = list()

        for x in poly_vertices:
            poly_vert_flat.append((x.long, x.lat))

        if len(poly_vert_flat) > 2:
            poly = Polygon(poly_vert_flat)
            ax.add_geometries([poly], ccrs.PlateCarree(), facecolor='#003366', alpha = 0.25)

        center = self.geodesic_point_buffer(host.lat, host.long, host.radius)
        poly = Polygon(b)
        ax.add_geometries([poly], ccrs.PlateCarree(), facecolor='#C8A2C8', alpha=0.25)

        centroid = Point(centroid.long, centroid.lat).buffer(0.1)
        ax.add_geometries([centroid], ccrs.PlateCarree(), facecolor='#000000', alpha = 1)

        # plt.show()

        return

    def genPlateCarree( self ):

        proj = ccrs.PlateCarree()

        fig = plt.figure(figsize=(self.dim[0]/self.dpi, self.dim[1]/self.dpi), dpi=self.dpi)
        ax = fig.add_subplot(1, 1, 1, projection=proj)



        return

    def genOrthographic( self ):

        proj = ccrs.Orthographic()

        fig = plt.figure(figsize=(self.dim[0]/self.dpi, self.dim[1]/self.dpi), dpi=self.dpi)
        ax = fig.add_subplot(1, 1, 1, projection=proj)



        return
