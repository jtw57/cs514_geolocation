"""

Projection Engine for Project 2
Constraint-Based Geolocation of Internet Hosts

Created by Justin Wang (jtw57) and Dhruv K Patel (dkp13)

Description: maps internet hosts and ranges onto a Mercator projection of Earth

"""

### Import Statements ###
import plotly
import plotly.plotly as py
from plotly.offline import init_notebook_mode
import plotly.graph_objs as go

import pandas as pd

plotly.offline.init_notebook_mode(connected=True)

class Projection:

    def __init__( self ):
        return

    def genMercator( self, hosts, poly_vertices, centroid ):

        locations = []
        colors = ["rgb(0,116,217)","rgb(255,65,54)","rgb(133,20,75)","rgb(255,133,27)","lightgrey"]

        for host in hosts:
            loc = dict(
                type = 'scattergeo',
                locationmode = 'USA-states',
                lon = [host.long],
                lat = [host.lat],
                opacity = 0.2,
                marker = dict(
                    size = 100,
                    # sizeref = 2. * max(df_sub['pop']/scale) / (25 ** 2),
                    color = colors[0],
                    line = dict(width=0.5, color='rgb(40,40,40)'),
                    sizemode = 'diameter'
                ))
            locations.append(loc)

        layout = dict(
                    title = 'Network Ping Ranges',
                    showlegend = True,
                    geo = dict(
                        scope='usa',
                        projection=dict( type='albers usa' ),
                        showland = True,
                        landcolor = 'rgb(217, 217, 217)',
                        subunitwidth=1,
                        countrywidth=1,
                        subunitcolor="rgb(255, 255, 255)",
                        countrycolor="rgb(255, 255, 255)"
                    ),
                )
        #
        # choromap = go.Figure(data = [data],layout = layout)

        fig = dict(data=locations, layout=layout)
        plotly.offline.plot(fig)
