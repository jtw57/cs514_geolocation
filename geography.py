import csv
from math import sin, cos, sqrt, atan2, radians


def countries_in_continent(continent):
    with open("data/country_codes.csv") as csvfile:
        countries = csv.reader(csvfile, delimiter=',', quotechar='\'')
        return [row[0] for row in countries if row[1] == continent]


# https://stackoverflow.com/questions/19412462/getting-distance-between-two-points-based-on-latitude-longitude
def geo_distance(lon1, lat1, lon2, lat2):
    r = 6373.0  # approximate radius of earth in km

    lon1 = radians(lon1)
    lat1 = radians(lat1)
    lon2 = radians(lon2)
    lat2 = radians(lat2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return r * c


