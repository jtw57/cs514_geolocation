"""

Multilateration Engine for Project 2
Constraint-Based Geolocation of Internet Hosts

Created by Justin Wang (jtw57) and Dhruv K Patel (dkp13)

Description: returns the vertices and centroid of the intersection polygon

"""

# Import Statements
import numpy as np
import pyproj
import shapely.ops as ops
from shapely.geometry.polygon import Polygon
from functools import partial

from projection import Projection
# from projection_prev import Projection as ProjectionPrev


class MultilaterationException(Exception):
    pass


class Multilateration:

    def __init__( self, lat_array, long_array, radii, country_code ):

        num_circles = lat_array.shape[0]

        self.coordinates = self.__extractData__(lat_array, long_array, radii)
        self.vertices = self.__calcVertices__(self.coordinates)
        self.poly_vertices = self.__calcPolyVertices__(self.vertices, num_circles)
        self.centroid = self.__calcCentroid__(self.poly_vertices)
        self.area = self.__calcArea__(self.poly_vertices)
        self.proj = Projection(country_code)

    def plot(self):
        self.proj.genMercator(self.coordinates, self.poly_vertices, self.centroid)

    def getVertices( self ):

        return self.vertices

    def getPolyVertices( self ):

        return self.poly_vertices

    def getCentroid( self ):

        return self.centroid

    def getHostCoordinates( self ):

        return self.coordinates

    def getArea( self ):

        return self.area

    # def plot( self, country_code ):
    #
    #     proj = Projection(country_code)
    #     proj.genMercator(self.coordinates, self.poly_vertices, self.centroid)

    def __pointOfInterest__(self, point_index, point, coordinates, vertex_coordinates):

        conc_num = 0

        for coordinate_index, x in enumerate(coordinates):

            delt_lat = np.deg2rad(point.lat - x.lat)
            delt_long = np.deg2rad(point.long - x.long)

            if delt_lat == 0 and delt_long == 0:
                continue

            a = np.sin(delt_lat/2)**2 + np.cos(np.deg2rad(point.lat)) * np.cos(np.deg2rad(x.lat)) * np.sin(delt_long/2)**2

            # c = 2 * np.arctan2(np.sqrt(np.absolute(a)), np.sqrt(1-np.absolute(a)))
            c = 2 * np.arcsin(np.sqrt(a))
            d = 6371 * c  # TODO: change for mi (3959)

            if (np.absolute(d) <= x.radius) or (coordinate_index in vertex_coordinates[point_index]):
                conc_num += 1

        point.setConcNum(conc_num)

    def __extractData__( self, lat_array, long_array, radii ):

        if lat_array.size != long_array.size or lat_array.size != radii.size:
            return None

        stacked_array = np.stack((lat_array, long_array, radii), axis = -1)
        coordinates = list()

        for x in stacked_array:
            coordinates.append(InternetHost(x[0], x[1], x[2]))

        return np.array(coordinates)

    def __calcVertices__( self, coordinates ):

        vertices = list()
        vertex_coordinates = []

        for x in xrange(coordinates.shape[0]):

            for y in xrange(x + 1, len(coordinates)):


                a = coordinates[x]
                b = coordinates[y]

                dot = np.dot(a.xyz, b.xyz)

                if dot == 1:
                    continue

                alpha = (np.cos(a.radian) - dot*np.cos(b.radian))/(1-dot**2)
                beta = (np.cos(b.radian) - dot*np.cos(a.radian))/(1-dot**2)

                x0 = alpha*a.xyz + beta*b.xyz
                n = np.cross(a.xyz, b.xyz)

                t_sqrt = (1-np.dot(x0, x0))/np.dot(n, n)

                if t_sqrt < 0:
                    continue

                t = np.sqrt(t_sqrt)

                xyz1 = x0 + t*n
                xyz2 = x0 - t*n

                vertices.append(Vertex(np.rad2deg(np.arctan2(xyz1[2], np.sqrt(xyz1[0]**2 + xyz1[1]**2))),
                                       np.rad2deg(np.arctan2(xyz1[1], xyz1[0]))))
                vertices.append(Vertex(np.rad2deg(np.arctan2(xyz2[2], np.sqrt(xyz2[0]**2 + xyz2[1]**2))),
                                       np.rad2deg(np.arctan2(xyz2[1], xyz2[0]))))
                vertex_coordinates.extend([[x, y], ]*2)

        for i, x in enumerate(vertices):

            self.__pointOfInterest__(i, x, coordinates, vertex_coordinates)

        return np.array(vertices)

    def __calcPolyVertices__( self, vertices, target ):

        if target == 0:

            return None

        poly_vertices = list()


        for x in vertices:
            if x.conc_num == target:
                poly_vertices.append(x)


        for x in poly_vertices:
            print "This is a vert: " + str(x.lat) + " " + str(x.long)

        return None if len(poly_vertices) < 3 else np.array(poly_vertices)

    def __calcCentroid__( self, poly_vertices):

        xyz_list = list()

        if poly_vertices is None:
            raise MultilaterationException("No overlapping polygon")

        for x in poly_vertices:
            xyz_list.append(x.gcs)

        xyz_arr = np.array(xyz_list)
        length = xyz_arr.shape[0]

        sum_lat = np.sum(xyz_arr[:, 0])
        sum_long = np.sum(xyz_arr[:, 1])

        return Vertex(sum_lat/length, sum_long/length)

    def __calcArea__( self, poly_vertices):

        poly_vert_flat = list()

        for x in poly_vertices:
            poly_vert_flat.append((x.long, x.lat))

        geom = Polygon(poly_vert_flat)
        geom_area = ops.transform(
            partial(
                pyproj.transform,
                pyproj.Proj(init='EPSG:4326'),
                pyproj.Proj(
                    proj='aea',
                    lat1=geom.bounds[1],
                    lat2=geom.bounds[3])
                ),
            geom
        )

        return geom_area.area / (10**6)

# Container for Internet Host Information
class InternetHost:

    def __init__( self, lat, long, radius ):

        rad_long = np.deg2rad(long)
        rad_lat = np.deg2rad(lat)

        self.lat = lat
        self.long = long

        self.x = np.cos(rad_long) * np.cos(rad_lat)
        self.y = np.sin(rad_long) * np.cos(rad_lat)
        self.z = np.sin(rad_lat)

        self.gcs = np.array([lat, long])
        self.xyz = np.array([self.x, self.y, self.z])

        self.radius = radius
        self.r_degrees = (radius*0.53995680)/60
        self.radian = np.deg2rad((radius*0.53995680)/60) # TODO: or 1.1508 for mi to NM

class Vertex:

    def __init__( self, lat, long ):

        self.lat = lat
        self.long = long
        self.gcs = np.array([lat, long])
        self.conc_num = 0

    def setConcNum( self, conc_num ):

        self.conc_num = conc_num

def testMult():

    # lats = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    # longs = np.array([101, 102, 103, 104, 105, 106, 107, 108, 109, 110])
    # radii = np.array([100, 200, 300, 400, 500, 600, 700, 800, 900, 1000])

    longs = np.array([-90.234036, -90.953669, -88.304678])
    lats = np.array([37.673442, 36.109997, 37.468361])
    radii = np.array([199.09, 268.54, 100])

    mult = Multilateration(lats, longs, radii)
    mult.plot('eu')

if __name__ == '__main__':
    testMult()
