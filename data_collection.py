from geography import *
from cbg import *
import requests
import json
import time
import random
import numpy as np
import toolz

FILE_ALL_ANCHORS = "data/all_anchors.json"
FILE_ALL_MEASUREMENTS = "data/all_measurements.json"
FILE_ANCHORS = "data/anchors.json"
FILE_MEASUREMENTS = "data/measurements.json"
FILE_RTTS = "data/rtts.json"

RIPE_URL = "https://atlas.ripe.net/api/v2/"


def get_anchors_from_country(country):
    url_path = RIPE_URL + "anchors/?country=" + country
    result = requests.get(url_path)
    content = json.loads(result.content)
    return content["results"]


def get_anchors_from_continent(continent):
    print "Querying anchors from {}...".format(continent)
    results = []
    countries = countries_in_continent(continent)
    for i, country in enumerate(countries):
        result = get_anchors_from_country(country)
        result = [r for r in result if not r["is_ipv4_only"]]  # only include IPv6 hosts
        results.extend(result)
        print("Retrieved %d anchors from %s:%s\t(%d of %d)"
              % (len(result), continent, country, i + 1, len(countries)))
    print("Complete. Total number of anchors:\t%d" % len(results))
    return results


def get_relevant_measurement_urls(anchors):
    measurements_url = RIPE_URL + "anchor-measurements/?include=measurement"
    measurements = []
    page = 1

    print "Querying anchor measurements..."
    while measurements_url:
        response = json.loads(requests.get(measurements_url).content)
        measurements.extend(response["results"])
        print("Retrieved measurements page:\t{}".format(page))
        measurements_url = response["next"]
        page += 1

    anchor_ids = set([a["fqdn"] for a in anchors])

    def is_relevant(measurement):
        relevant_anchor = measurement["measurement"]["target"] in anchor_ids  # ensure measured anchor is in sample
        relevant_type = measurement["type"] == "ping"  # ensure measurement is a ping
        relevant_ip_version = measurement["measurement"]["af"] == 4  # ensure ping is ipv4 (more anchors supported)
        relevant_description = measurement["is_mesh"]  # ensure measurement is "mesh", not "probe"
        relevant_state = measurement["measurement"]["status"]["name"] == "Ongoing"  # measurement still running
        return relevant_anchor and relevant_type and relevant_ip_version and relevant_description and relevant_state
    measurements = [m for m in measurements if is_relevant(m)]

    print("Complete. Total number of ongoing measurements:\t%d" % len(measurements))
    return measurements


def get_rtts_between_anchors(anchors, measurements, start_time, end_time):
    rtts = np.full((len(anchors),) * 2, -1, dtype=float)
    rtts_all = [[[] for _ in range(len(anchors))] for _ in range(len(anchors))]

    start_time_unix = str(int(time.mktime(start_time.timetuple())))
    end_time_unix = str(int(time.mktime(end_time.timetuple())))
    index_by_probe_id = dict(zip([a["probe"] for a in anchors], range(len(anchors))))

    print("Querying pings for {} anchors...".format(len(anchors)))
    for anchor, measurement, i_anchor in zip(anchors, measurements, range(len(anchors))):
        targets = list(filter(lambda target: target["fqdn"] != anchor["fqdn"], anchors))

        probe_ids_string = ""
        for i, target in enumerate(targets):
            probe_ids_string += ("%2C{}" if i > 0 else "{}").format(target["probe"])

        url_path = "{url}measurements/{pk}/results/?start={start}&stop={stop}&probe_ids={probe_ids}&anchors-only=true"\
            .format(url=RIPE_URL,
                    pk=measurement["measurement"]["id"],
                    start=start_time_unix,
                    stop=end_time_unix,
                    probe_ids=probe_ids_string)

        result = requests.get(url_path)
        pings = json.loads(result.content)

        for ping in pings:
            i_target = index_by_probe_id[ping["prb_id"]]
            rtt = ping["min"]
            if rtt != -1:
                rtts_all[i_anchor][i_target].append(rtt)

        print ("pong" if i_anchor % 2 else "ping") + "\t({} of {})".format(i_anchor + 1, len(anchors))

    for r in range(len(rtts_all)):
        for c in range(len(rtts_all[r])):
            rtts[r][c] = get_rtt_from_list(rtts_all[c][r])

    return rtts


def filter_anchors_by_rtt(anchors, measurements, distances, rtts):
    """
    Removes any anchor with RTT column or row of all "-1".
    Essentially, it removes anchors without valid ping measurements.
    Note: this only removes anchor if ping is invalid on connection
    with ALL other anchors
    """
    size = rtts.shape[0]
    invalids = (rtts == -1).astype(int)
    valid_anchors = list(np.where(np.logical_and(np.sum(invalids, axis=0) != size,
                                                 np.sum(invalids, axis=1) != size))[0])

    anchors = [anchors[i] for i in valid_anchors]
    measurements = [measurements[i] for i in valid_anchors]
    distances = distances[valid_anchors, :][:, valid_anchors]
    rtts = rtts[valid_anchors, :][:, valid_anchors]

    return anchors, measurements, distances, rtts


def get_distances_between_anchors(anchors):
    size = (len(anchors),)*2
    anchor_coordinates = np.array([np.array(anchor["geometry"]["coordinates"]) for anchor in anchors])
    indices = np.indices(size)

    coordinate_src = np.take(anchor_coordinates, indices[0].flatten(), axis=0)
    coordinate_dest = np.take(anchor_coordinates, indices[1].flatten(), axis=0)
    coordinate_pairs = np.column_stack((coordinate_src, coordinate_dest))

    distances_vec = np.apply_along_axis(lambda row: geo_distance(*row), axis=1, arr=coordinate_pairs)
    distances = np.reshape(distances_vec, size)
    return distances


def remove_anchors_without_measurement(anchors, measurements):
    measurements_fqdn = set([m["measurement"]["target"] for m in measurements])
    anchors = list(filter(lambda a: a["fqdn"] in measurements_fqdn, anchors))
    return anchors


def remove_measurements_without_anchors(anchors, measurements):
    anchors_fqdn = set([a["fqdn"] for a in anchors])
    measurements = list(filter(lambda m: m["measurement"]["target"] in anchors_fqdn, measurements))
    return measurements


def sample_and_sort(anchors, measurements, sample_size):
    anchors = random.sample(anchors, sample_size) if sample_size is not -1 else anchors
    anchors_fqdn = set([a["fqdn"] for a in anchors])
    measurements = list(filter(lambda m: m["measurement"]["target"] in anchors_fqdn, measurements))

    fqdn_to_measurement = dict(zip([m["measurement"]["target"] for m in measurements], measurements))

    order = sorted(range(len(anchors)), key=(lambda i: anchors[i]["fqdn"]))
    anchors = [anchors[i] for i in order]
    measurements = [fqdn_to_measurement[a["fqdn"]] for a in anchors]

    return anchors, measurements


def save_json(data, filename):
    with open(filename, 'w') as f:
        json.dump(data, f, indent=4)


def load_as_json(filename):
    with open(filename, 'r') as f:
        return json.load(f)


def collect_data(continent, anchor_sample_size, start_time, end_time, do_get_anchors, do_sample, do_pings):
    assert (not do_get_anchors and not do_sample) or (do_sample and do_pings), "must complete previous processes " \
                                                                               "for all stages in pipline"

    # Get all Anchors & Measurements in continent
    if do_get_anchors:
        all_anchors = get_anchors_from_continent(continent)
        all_measurements = get_relevant_measurement_urls(all_anchors)

        save_json(all_anchors, FILE_ALL_ANCHORS)
        save_json(all_measurements, FILE_ALL_MEASUREMENTS)

    else:
        all_anchors = load_as_json(FILE_ALL_ANCHORS)
        all_measurements = load_as_json(FILE_ALL_MEASUREMENTS)

    # Takes random sample of all usable anchors
    if do_sample:
        # Filter and sample
        all_anchors = list(toolz.unique(all_anchors, key=lambda a: a["fqdn"]))
        all_measurements = list(toolz.unique(all_measurements, key=lambda m: m["measurement"]["target"]))

        all_anchors = remove_anchors_without_measurement(all_anchors, all_measurements)
        all_measurements = remove_measurements_without_anchors(all_anchors, all_measurements)

        anchors, measurements = sample_and_sort(all_anchors, all_measurements, anchor_sample_size)

        # Save samples
        save_json(anchors, FILE_ANCHORS)
        save_json(measurements, FILE_MEASUREMENTS)

    else:
        anchors = load_as_json(FILE_ANCHORS)
        measurements = load_as_json(FILE_MEASUREMENTS)

    # Get RTTs
    if do_pings:
        rtts = get_rtts_between_anchors(anchors, measurements, start_time, end_time)  # ms
        save_json(rtts.tolist(), FILE_RTTS)

    else:
        rtts = np.array(load_as_json(FILE_RTTS))

    distances = get_distances_between_anchors(anchors)  # km
    anchors, measurements, distances, rtts = filter_anchors_by_rtt(anchors, measurements, distances, rtts)

    return anchors, measurements, distances, rtts
